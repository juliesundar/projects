
/*********

  Rui Santos

  Complete project details at https://randomnerdtutorials.com  

*********/

// Motor A

int motor1Pin1 = 12; 

int motor1Pin2 = 14; 

int enable1Pin = 25; 

// Motor B

int motor2Pin3 = 18; 

int motor2Pin4 = 23; 

int enable2Pin = 15; 

// Setting PWM properties

const int freq = 30000;

const int pwmChannel = 0;

const int resolution = 8;

int dutyCycle = 200;

void setup() {

  // sets the pins as outputs:

  pinMode(motor1Pin1, OUTPUT);

  pinMode(motor1Pin2, OUTPUT);

  pinMode(enable1Pin, OUTPUT);

   // sets the pins as outputs:

  pinMode(motor2Pin3, OUTPUT);

  pinMode(motor2Pin4, OUTPUT);

  pinMode(enable2Pin, OUTPUT);

  // configure LED PWM functionalitites

  ledcSetup(pwmChannel, freq, resolution);

  

  // attach the channel to the GPIO to be controlled

  ledcAttachPin(enable1Pin, pwmChannel);

  ledcAttachPin(enable2Pin, pwmChannel);

  Serial.begin(115200);

  // testing

  Serial.print("Testing DC Motor...");

}

void loop() {

  // Move the DC motor forward at maximum speed

  Serial.println("Moving Forward");

  digitalWrite(motor1Pin1, LOW);

  digitalWrite(motor1Pin2, HIGH); 

  ledcWrite(pwmChannel, 200);

  delay(2000);

  digitalWrite(motor2Pin3, LOW);

  digitalWrite(motor2Pin4, HIGH); 

  ledcWrite(pwmChannel, 200);

  delay(2000);

  

  // Stop the DC motor

  Serial.println("Motor stopped");

  digitalWrite(motor1Pin1, LOW);

  digitalWrite(motor1Pin2, LOW);

  ledcWrite(pwmChannel, 0);

  delay(1000);

  // Stop the DC motor

  Serial.println("Motor stopped");

  digitalWrite(motor2Pin3, LOW);

  digitalWrite(motor2Pin4, LOW);

  ledcWrite(pwmChannel, 0);

  delay(1000);

  

  // Move DC motor backwards at maximum speed

  Serial.println("Moving Backwards");

  digitalWrite(motor1Pin1, HIGH);

  digitalWrite(motor1Pin2, LOW);

  ledcWrite(pwmChannel, 200); 

  delay(2000);

  // Stop the DC motor

  Serial.println("Motor stopped");

  digitalWrite(motor1Pin1, LOW);

  digitalWrite(motor1Pin2, LOW);

  ledcWrite(pwmChannel, 0);

  delay(1000);

  // Move DC motor forward with increasing speed

  digitalWrite(motor1Pin1, HIGH);

  digitalWrite(motor1Pin2, LOW);

  while (dutyCycle <= 255){

    ledcWrite(pwmChannel, dutyCycle);   

    Serial.print("Forward with duty cycle: ");

    Serial.println(dutyCycle);

    dutyCycle = dutyCycle + 5;

    delay(500);

  }

  dutyCycle = 200;

}
