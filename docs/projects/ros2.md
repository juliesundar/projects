### ROS2 Robotic Operating System  ONLINE TRAINING 

Sibarit7!

[https://ubuntu.com/download/server/arm](https://ubuntu.com/download/server/arm) 

Download utuntu image

Instaling on raspberry pi 3

Image ubuntu 20.04 raspberry pi 3 64 bit

**UBUNTU USER NAME AND PASS**

User: ubuntu

Pass: ubuntu 

NOTE: PATIENCE!!  Wait until cloud init returns key before u can login if not you will get incorrect login

**## HEADLESS**

[https://roboticsbackend.com/install-ubuntu-on-raspberry-pi-without-monitor/](https://roboticsbackend.com/install-ubuntu-on-raspberry-pi-without-monitor/) 

**Setup Wi-Fi directly from your SD card**


Navigate inside the root folder of the card. The name should be something like system-boot.

Find the file named network-config and open it in a text editor. On Windows, you can right-click -> open with and select any text editor you want.

The file should contains this:

# This file contains a netplan-compatible configuration which cloud-init

# will apply on first-boot. Please refer to the cloud-init documentation and

# the netplan reference for full details:

# https://cloudinit.readthedocs.io/

# https://netplan.io/reference


# Some additional examples are commented out below

version: 2

ethernets:

  eth0:

    dhcp4: true

    optional: true

#wifis:

#  wlan0:

#    dhcp4: true

#    optional: true

#    access-points:

#      myhomewifi:

#        password: "S3kr1t"

#      myworkwifi:

#        password: "correct battery horse staple"

#      workssid:

#        auth:

#          key-management: eap

#          method: peap

#          identity: "me@example.com"

#          password: "passw0rd"

#          ca-certificate: /etc/my_ca.pem

version: 2

ethernets:

  eth0:

    dhcp4: true

    optional: true

wifis:

  wlan0:

    dhcp4: true

    optional: true

    access-points:

      "YOUR_WIFI_NAME":

        password: "YOUR_WIFI_PASSWORD"

**### SSH STUFF HERE**

**ssh setup**


For ssh, you should not need to do anything.

If you go into the user-data file, also inside the root directory of your SD card, you should see:

# On first boot, set the (default) ubuntu user's password to "ubuntu" and

# expire user passwords

chpasswd:

  expire: true

  list:

  - ubuntu:ubuntu

# Enable password authentication with the SSH daemon

ssh_pwauth: true

**#### ROS STUFF HERE**

[https://index.ros.org/doc/ros2/Installation/Foxy/Linux-Development-Setup/](https://index.ros.org/doc/ros2/Installation/Foxy/Linux-Development-Setup/) 


```
locale  # check for UTF-8

sudo apt update && sudo apt install locales
sudo locale-gen en_US en_US.UTF-8
sudo update-locale LC_ALL=en_US.UTF-8 LANG=en_US.UTF-8
export LANG=en_US.UTF-8

locale  # verify settings
```


[Setup Sources](https://index.ros.org/doc/ros2/Installation/Foxy/Linux-Install-Debians/#id3)


You will need to add the ROS 2 apt repositories to your system. To do so, first authorize our GPG key with apt like this:


```
sudo apt update && sudo apt install curl gnupg2 lsb-release
curl -s https://raw.githubusercontent.com/ros/rosdistro/master/ros.asc | sudo apt-key add -
```


And then add the repository to your sources list:


```
sudo sh -c 'echo "deb [arch=$(dpkg --print-architecture)] http://packages.ros.org/ros2/ubuntu $(lsb_release -cs) main" > /etc/apt/sources.list.d/ros2-latest.list'
```


[Install ROS 2 packages](https://index.ros.org/doc/ros2/Installation/Foxy/Linux-Install-Debians/#id4)


Update your apt repository caches after setting up the repositories.


```
sudo apt update
```


Desktop Install (Recommended): ROS, RViz, demos, tutorials.


```
sudo apt install ros-foxy-desktop
sudo apt install ros-foxy-ros-base
```


<h3>[Environment setup](https://index.ros.org/doc/ros2/Installation/Foxy/Linux-Install-Debians/#id5)</h3>


<h4>[Sourcing the setup script](https://index.ros.org/doc/ros2/Installation/Foxy/Linux-Install-Debians/#id6)</h4>


Set up your environment by sourcing the following file.


```
source /opt/ros/foxy/setup.bash
```


<h4>[Install argcomplete (optional)](https://index.ros.org/doc/ros2/Installation/Foxy/Linux-Install-Debians/#id7)</h4>


ROS 2 command line tools use argcomplete to autocompletion. So if you want autocompletion, installing argcomplete is necessary.


```
sudo apt install -y python3-pip
pip3 install -U argcomplete
```


<h4>[Install development tools and ROS tools](https://index.ros.org/doc/ros2/Installation/Foxy/Linux-Development-Setup/#id5)</h4>


**COLCON **


```
sudo apt update && sudo apt install -y \
  build-essential \
  cmake \
  git \
  libbullet-dev \
  python3-colcon-common-extensions \
  python3-flake8 \
  python3-pip \
  python3-pytest-cov \
  python3-rosdep \
  python3-setuptools \
  python3-vcstool \
  Wget

# install some pip packages needed for testing
python3 -m pip install -U argcomplete flake8-blind-except flake8-builtins flake8-class-newline  flake8-comprehensions flake8-deprecated flake8-docstrings flake8-import-order flake8-quotes pytest-repeat pytest-rerunfailures pytest
```


<h3>[Get ROS 2 code](https://index.ros.org/doc/ros2/Installation/Foxy/Linux-Development-Setup/#id6)</h3>


Create a workspace and clone all repos:

**mkdir -p ~/ros2_ws/src**


```
mkdir -p ~/ros2_foxy/src
cd ~/ros2_foxy
colcon build
BUILD INSTALL LOG SRC
```


![alt_text](images/image1.png "image_tooltip")



![alt_text](images/image2.png "image_tooltip")


ALL ROS FILES ARE INSTALLED IN opt/ros/bouncy

sudo apt install gedit 

Enable XLL ON PUTTY 

NOTE: INSTALL XMING AND ENABLE X11 ON PUTTY

MAKE SURE XMING IS RUNNING BEFORE LAUNCHING COMMMAND  gedit.bashrc



![alt_text](images/image3.png "image_tooltip")


PUTTY ? SSH ? X11 ? X11 ENABLE 


![alt_text](images/image4.png "image_tooltip")


 gedit .bashrc       note: do not add sudo before this command it will not open window display

Add to bashrc file 

source /root/ubuntu/ros2_foxy/install/local_setup.bash

source /root/ubuntu/ros2_foxy/install/setup.bash

Note: localsetup.bash sets up local workspace & setup.bash sets up parent and local workspace. Parent workspace is located in opt/ros/ folder 

Navigate to ros example on git lab [https://github.com/ros2/examples](https://github.com/ros2/examples)

Navigate to src folder  ros2_foxy/src git clone [https://github.com/ros2/examples.git](https://github.com/ros2/examples.git) 


![alt_text](images/image5.png "image_tooltip")


Colcon build --symlink-install    create all the executable files to run ros nodes that are in the example packages


![alt_text](images/image6.png "image_tooltip")



```
sudo -s  /usr/lib/sftp-server

source /opt/ros/foxy/setup.bash 

ros2 run turtlesim turtlesim_node
source /opt/ros/foxy/setup.bash 
ros2 run turtlesim turtle_teleop_key
```


**### THEO LINK ROS2 PI3 UBUNTU 18 **

[https://medium.com/realroboox/how-to-setup-ros2-on-raspberry-pi-3-model-b-6b77cd350b7a](https://medium.com/realroboox/how-to-setup-ros2-on-raspberry-pi-3-model-b-6b77cd350b7a) 
