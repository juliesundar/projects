#include <Wire.h>

#include <LiquidCrystal_I2C.h>

#include "DHT.h"

#define DHTPIN 7 

#define PWMPIN 3

#define DIRA 4

#define DIRB 5

//#define DHTPIN 7     // what pin we're connected to

LiquidCrystal_I2C lcd = LiquidCrystal_I2C(0x27, 16, 2);

byte arms[8] = {

  0b00100,

  0b01010,

  0b00100,

  0b00100,

  0b01110,

  0b10101,

  0b00100,

  0b01010

};

// Uncomment whatever type you're using!

#define DHTTYPE DHT11   // DHT 11 

// Initialize DHT sensor for normal 16mhz Arduino

DHT dht(DHTPIN, DHTTYPE);

void setup() {

  Serial.begin(9600); 

  dht.begin();

  Wire.begin();

  lcd.init();

  lcd.backlight();

  lcd.createChar(1, arms);  

  pinMode(PWMPIN,OUTPUT);

  pinMode(DIRA,OUTPUT);

  pinMode(DIRB,OUTPUT);

}

void loop() {

  // Wait a few seconds between measurements.

  delay(2000);

  // Reading temperature or humidity takes about 250 milliseconds!

  // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)

  float h = dht.readHumidity();

  // Read temperature as Celsius

  float t = dht.readTemperature();

  // Read temperature as Fahrenheit

  float f = dht.readTemperature(true);

  

  // Check if any reads failed and exit early (to try again).

  if (isnan(h) || isnan(t) || isnan(f)) {

    Serial.println("Failed to read from DHT sensor!");

    return;

  }

  // Compute heat index

  // Must send in temp in Fahrenheit!

  float hi = dht.computeHeatIndex(f, h);

  Serial.print("Humidity: "); 

  Serial.print(h);

  Serial.print(" %\t");

  Serial.print("Temperature: "); 

  Serial.print(t);

  Serial.print(" *C ");

  Serial.print(f);

  Serial.println(" *F\t");

  if(t>30)

    {   lcd.setCursor(0, 0);

        lcd.print("T:");

        lcd.print(t);

        lcd.setCursor(8, 0);

        lcd.print("H:");

        lcd.print(h);

        lcd.setCursor(7, 1);

        lcd.print(" ON ");

        lcd.write(1); 

        analogWrite(PWMPIN,200); // enable on

        digitalWrite(DIRA,HIGH); //one way

        digitalWrite(DIRB,LOW);

        delay(20);

        lcd.setCursor(0, 1);

        lcd.print("FAN");

        lcd.setCursor(4, 1);

        lcd.print("1");

        delay(1000);

    }

   if(t<30)

    {

        lcd.setCursor(0, 0);

        lcd.print("T:");

        lcd.print(t);

        lcd.setCursor(8, 0);

        lcd.print("H:");

        lcd.print(h);

        lcd.setCursor(7, 1);

        lcd.print(" ON ");

        lcd.write(1); 

        analogWrite(PWMPIN,100); // enable on

        digitalWrite(DIRA,HIGH); //one way

        digitalWrite(DIRB,LOW);

        delay(10);

        lcd.setCursor(0, 1);

        lcd.print("FAN");

        lcd.setCursor(4, 1);

        lcd.print("2");

        delay(1000);

    }

    if(t<25)

    {  

        lcd.setCursor(0, 0);

        lcd.print("T:");

        lcd.print(t);

        lcd.setCursor(8, 0);

        lcd.print("H:");

        lcd.print(h);

        lcd.setCursor(7, 1);

        lcd.print(" FAN OFF");

        analogWrite(PWMPIN,0); // enable on

        digitalWrite(DIRA,HIGH); //one way

        digitalWrite(DIRB,LOW);

        delay(20);

        lcd.setCursor(0, 1);

        lcd.print("3"); 

        delay(1000);

    }

}
