
* Source doc: Raspberry Nodejs Python
----->


**[https://caffinc.github.io/2016/12/raspberry-pi-3-headless/](https://caffinc.github.io/2016/12/raspberry-pi-3-headless/) **

**[https://hackernoon.com/raspberry-pi-headless-install-462ccabd75d0](https://hackernoon.com/raspberry-pi-headless-install-462ccabd75d0) **

**full detailed too much info**

**Tools: **

Putty : http://www.putty.org/

Winscp : https://sourceforge.net/projects/winscp/files/latest/download

Remote Desktop : apt-get install xrdp on rpi 

_Note: Write image to device or device to image_

Windiskimager : https://sourceforge.net/projects/win32diskimager/

**Linux distribution**

[https://www.raspberrypi.org/downloads/raspbian/](https://www.raspberrypi.org/downloads/raspbian/) 

Raspbian Wheezy if using rpi2

Raspbian Jessie if using rpi3

_Use WinDiskImager to write image to sd (use min 8gb sd card if using opencv use 16gb)_

_Cmd to check os version_

_cat /etc/os-release_

**IP Address **

_Quick & Dirty _

Edit commandline.txt file after inserting sd into pc  ip=192.168.1.151 (your choice) at the end.

Enable ssh by adding an file called **ssh **in the root with no extension

Create a file in this directory called wpa_supplicant.conf. The file should contain the following details:


```
network={
    ssid="YOUR_NETWORK_NAME"
    psk="YOUR_PASSWORD"
    key_mgmt=WPA-PSK
}
```


**Nice to have:**

Network mapping tools : 

Open cmd ping 192.168.1.151 (your choice) to check if it is up

_Angry ip scanner or nmap tools _

Create a telnet session with putty  with ip address

**Log in**

User : pi  

Password: raspberry 

Sudo raspi-**config (enable ssh)**

Once log in I recommend enabling root user 

sudo su

_apt-get install xrdp_ (to log in from remote desktop)

I_f getting errors:_


```
sudo apt-get install vnc4server, followed by either a reboot or a sudo service xrdp restart
```


**Basic commands **



*   **mkdir**  make directories
*   Usage: mkdir [OPTION] DIRECTORY
*   eg. mkdir lhn
*   **ls**  list directory contents
*   Usage: ls [OPTION] [FILE]
*   eg. ls, ls l, ls lhn
*   **cd**  changes directories
*   Usage: cd [DIRECTORY]
*   eg. cd lhn
*   **pwd** -  print name of current working directory
*   Usage: pwd

**Activating Root user**


```
sudo nano /etc/ssh/sshd_config
sudo service
```


Change PermitRootLogin to yes

_# Authentication:_

_LoginGraceTime 120_

_#PermitRootLogin without-password_

_PermitRootLogin yes_

_StrictModes yes_

Ctrl X then Y  to save 

sudo passwd root

Choose password

Exit putty session

Login as root instead of pi  

**Install webmin**

**http://www.instructables.com/id/Adding-Webmin-to-manage-a-Raspberry-Pi/**

wget [http://prdownloads.sourceforge.net/webadmin/webmin_1.760_all.deb](http://prdownloads.sourceforge.net/webadmin/webmin_1.760_all.deb)

sudo dpkg --install webmin_1.760_all.deb 

Apt-get install -f

**Install**

**Python**

_apt-get update _

_apt-get upgrade_

_apt-get install python_

_apt-get install python-pip_

**_Nodejs New (pi3 raspbian jessie_**



*   **<code><em>$ curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash - sudo apt install nodejs</em></code></strong>
*   <strong><code><em>$ node -v</em></code></strong>
*   <strong><code><em>$ npm -v</em></code></strong>

<strong><em>Nodejs OLD</em></strong>

**_[http://weworkweplay.com/play/raspberry-pi-nodejs/](http://weworkweplay.com/play/raspberry-pi-nodejs/)_**

_###wget http://node-arm.herokuapp.com/node_latest_armhf.deb _

_###sudo dpkg -i node_latest_armhf.deb_

_###INSTALL NPM apt-get install npm_

_node -v_

**nodejs folder structure**



*   root:
*   	nodejs
        *   projects
            *   Project1
                *   Public
                    *   Css
                    *   Js
                    *   Img
                    *   Index.html
                    *   server.js
            *   project2
        *   node_modules
            *   Express
            *   Socket.io
            *   Http 

**Pi camera module**

_[https://www.raspberrypi.org/documentation/usage/camera/raspicam/raspistill.md](https://www.raspberrypi.org/documentation/usage/camera/raspicam/raspistill.md)_

_FORMAT SD:_

_Sd formatter first_

_Then format sd card manually _

_[http://www.instructables.com/id/Raspberry-Pi-Arduino-Serial-Communication/](http://www.instructables.com/id/Raspberry-Pi-Arduino-Serial-Communication/) _

_Nodejs NVM (for development in different versions of Node)_

_(https://www.losant.com/blog/how-to-install-nodejs-on-raspberry-pi)_

_Install NVM_

_ curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.31.0/install.sh | bash_

_Use with:_

_nvm install v5.7.0 (or any other version u wanna use)_

_reboot_

_nvm install 9.11.1_

_nvm use 9.11.1 (or any)_

 

Install SerialPort on Rpi in Root

<h5>**sudo / root**</h5>


Remove old folder first

> rm -rf serialport 

If you're going to use `sudo` or root to install Node-Serialport, `npm` will require you to use the unsafe parameters flag.

npm install serialport --unsafe-perm --build-from-source

npm install livecam --unsafe-perm --build-from-source

npm install socket.io --unsafe-perm --build-from-source

npm install mqtt --unsafe-perm --build-from-source

Installing Motion 

apt-get install motion 

Next we need to enable the Daemon (service):

$ sudo nano /etc/default/motion

start_motion_daemon = no (change to yes)

 sudo nano /etc/motion/motion.conf

This is quite a large conf file but the points you need to edit are:

DAEMON = OFF (change to ON)

Webcam_localhost = ON (Change to OFF)

**stream_localhost off**

Remove motion

sudo apt-get purge motion

Start motion

_SUDO MOTION SERVICE START _

_NOT SUDO SERVICE MOTION START_

_Start motion_

_ sudo service motion  stop_

_192.168.1.151:8081_
