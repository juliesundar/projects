
#include <Servo.h>

Servo servo1;

int trigPin = 10;

int echoPin = 11;

int servoPin = 9;

long distance;

long duration;

void setup() 

{

  servo1.attach(servoPin); 

  servo1.write(90);

  pinMode(trigPin, OUTPUT);

  pinMode(echoPin, INPUT);

  Serial.begin(9600);

}

void loop()

{ 

   Serial.print("Distance: ");

   Serial.print(distance);

   Serial.println("  cm");

   ultra();

   if(distance <20 && distance >0 )

   {

        Serial.println("Dispensing soap");  

        servo1.write(55); 

        delay(2000); 

        servo1.write(90);

        delay(3000);

}

}

void ultra()

{

   digitalWrite(trigPin, LOW);

   delayMicroseconds(2);

   digitalWrite(trigPin, HIGH);

   delayMicroseconds(10);

   digitalWrite(trigPin, LOW);

  duration = pulseIn(echoPin, HIGH);

  distance = duration*0.034/2;

  }
