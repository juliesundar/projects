## **Kicad & Flatcam with Chinese CNC**



1. Import brd file into Kicad as Eagle Cad 
2. Place Auxiliary Axis 
3. Plot -- make sure what layer u like is checked off on top left and select use auxiliary axis as origin
4. On the same panel at the bottom Generate Drill Files and select Auxillary as origin on pop up screen then generate 


**FLATCAM**

Make sure mm is selected on both Application and Project Options


**Flatcam : Gerber params **


1. **SMOOTHIELASER_BREAKOUT-Bottom.gbr**
1. Tool dia : 0.15
2. Passes 1 
note: remove combine pass 
3. Generate Geometry 
4. **SMOOTHIELASER_BREAKOUT-Bottom.gbr_iso**
5. Cutz - 0.05 
6. Travel z 1.5
7. Spindel speed 10000
8. Feedrate 30 


**Excellon Object**

**SMOOTHIELASER_BREAKOUT-PTH.drl**

Cutz -1.8

Travelz 1.5

Feedrate 25

Spindle speed 10000




![alt_text](images/image4.png "image_tooltip")
