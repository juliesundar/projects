
    **Embeed Youtube Playlist**

1. Sign in to your YouTube account on a computer.
2. On the left side of the page, select the playlist you want to embed.
3. Copy the playlist ID from the URL.
4. Modify the embed code for an individual video by doing the following:
    1. In the place of the video ID (after "embed/"), substitute "videoseries?list=".
    2. Then paste the playlist ID after the "=".
    3. Paste the code into your blog or website HTML.

Example:

1. <iframe width="560" height="315" src="https://www.youtube.com/embed/videoseries?list=**PLx0sYbCqOb8TBPRdmBHs5Iftvv9TPboYG**" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
