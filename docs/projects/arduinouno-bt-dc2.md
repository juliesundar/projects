<!-- Copy and paste the converted output. -->


<img src="images/image1.png" width="" alt="alt_text" title="image_tooltip">
</p>
</h2>


<h2>InnoStarter </h2>


<h2>Motor Controlling with HC-05</h2>


<h4>_This tutorial explains how to control DC motor using Bluetooth module HC-05 with Arduino Uno._</h4>


<h5>1. Introduction </h5>


The Motor Driver is a module for motors that allows you to control the working speed and direction of two motors simultaneously .This Motor Driver is designed and developed based on L293D IC.

In this tutorial, we can control rotation of motors with bluetooth app by using HC_05 module.





![alt_text](images/image3.jpg "image_tooltip")


<h5> 2.  Hardware required</h5>



<table>
  <tr>
   <td>
   </td>
   <td>
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>1
   </td>
   <td><a href="https://roboindia.com/store/Arduino-accessories-development-Boards-embedded-robotic-system-diecimila-uno-projects/low-cost-arduino-uno-r3">Arduino UNO</a>
   </td>
   <td> 1
   </td>
  </tr>
  <tr>
   <td>2
   </td>
   <td>L293d
   </td>
   <td> 1
   </td>
  </tr>
  <tr>
   <td>3
   </td>
   <td><a href="https://roboindia.com/store/BO-Motors-90-Degrees?search=bo%20motor">DC Motor</a>
   </td>
   <td> 2
   </td>
  </tr>
  <tr>
   <td>4
   </td>
   <td><a href="https://roboindia.com/store/male-female-connectors-jumper-wire-electrical-electronics-embedded-system-prototype?product_id=253">Female to Male Jumper wire</a>
   </td>
   <td> 12
   </td>
  </tr>
  <tr>
   <td>5
   </td>
   <td><a href="https://roboindia.com/store/6xaa-battery-holder-black">9-12v Battery</a>
   </td>
   <td> 1
   </td>
  </tr>
  <tr>
   <td>6
   </td>
   <td><a href="https://roboindia.com/store/HC06-Bluetooth-module-wireless-BT?search=hc-05">HC-05</a>
   </td>
   <td>
   </td>
  </tr>
</table>


APP LINK [https://play.google.com/store/apps/details?id=com.keuwl.arduinobluetooth](https://play.google.com/store/apps/details?id=com.keuwl.arduinobluetooth) 



![alt_text](images/image4.jpg "image_tooltip")





![alt_text](images/image5.jpg "image_tooltip")





![alt_text](images/image6.jpg "image_tooltip")


/*

 * created by Rui Santos, https://randomnerdtutorials.com

 * Control 2 DC motors with Smartphone via bluetooth

 */

 

int motor1Pin1 = 3; // pin 2 on L293D IC

int motor1Pin2 = 4; // pin 7 on L293D IC

int enable1Pin = 2; // pin 1 on L293D IC

int motor2Pin1 = 9; // pin 10 on L293D IC

int motor2Pin2 = 10; // pin 15 on L293D IC

int enable2Pin = 8; // pin 9 on L293D IC

int state;

int flag=0;        //makes sure that the serial only prints once the state

int stateStop=0;

void setup() {

    // sets the pins as outputs:

    pinMode(motor1Pin1, OUTPUT);

    pinMode(motor1Pin2, OUTPUT);

    pinMode(enable1Pin, OUTPUT);

    pinMode(motor2Pin1, OUTPUT);

    pinMode(motor2Pin2, OUTPUT);

    pinMode(enable2Pin, OUTPUT);

    // sets enable1Pin and enable2Pin high so that motor can turn on:

    digitalWrite(enable1Pin, HIGH);

    digitalWrite(enable2Pin, HIGH);

    // initialize serial communication at 9600 bits per second:

    Serial.begin(9600);

}

void loop() {

    //if some date is sent, reads it and saves in state

    if(Serial.available() > 0){     

      state = Serial.read();   

      flag=0;

    }   

    // if the state is 'F' the DC motor will go forward

    if (state == 'F') {

        digitalWrite(motor1Pin1, HIGH);

        digitalWrite(motor1Pin2, LOW); 

        digitalWrite(motor2Pin1, LOW);

        digitalWrite(motor2Pin2, HIGH);

        if(flag == 0){

          Serial.println("Go Forward!");

          flag=1;

        }

    }

    

    // if the state is 'R' the motor will turn left

    else if (state == 'R') {

        digitalWrite(motor1Pin1, HIGH); 

        digitalWrite(motor1Pin2, LOW); 

        digitalWrite(motor2Pin1, LOW);

        digitalWrite(motor2Pin2, LOW);

        if(flag == 0){

          Serial.println("Turn LEFT");

          flag=1;

        }

        delay(1500);

        state=3;

        stateStop=1;

    }

    // if the state is 'S' the motor will Stop

    else if (state == 'S' || stateStop == 1) {

        digitalWrite(motor1Pin1, LOW); 

        digitalWrite(motor1Pin2, LOW); 

        digitalWrite(motor2Pin1, LOW);

        digitalWrite(motor2Pin2, LOW);

        if(flag == 0){

          Serial.println("STOP!");

          flag=1;

        }

        stateStop=0;

    }

    // if the state is 'L' the motor will turn right

    else if (state == 'L') {

        digitalWrite(motor1Pin1, LOW); 

        digitalWrite(motor1Pin2, LOW); 

        digitalWrite(motor2Pin1, LOW);

        digitalWrite(motor2Pin2, HIGH);

        if(flag == 0){

          Serial.println("Turn RIGHT");

          flag=1;

        }

        delay(1500);

        state=3;

        stateStop=1;

    }

    // if the state is 'B' the motor will Reverse

    else if (state == 'B') {

        digitalWrite(motor1Pin1, LOW); 

        digitalWrite(motor1Pin2, HIGH);

        digitalWrite(motor2Pin1, HIGH);

        digitalWrite(motor2Pin2, LOW);

        if(flag == 0){

          Serial.println("Reverse!");

          flag=1;

        }

    }

    //For debugging purpose

    //Serial.println(state);

}




![alt_text](images/image7.png "image_tooltip")





![alt_text](images/image8.jpg "image_tooltip")


Parts list: 

1x Breadboard

1x Arduino Uno

2x Mini Dc motor

1x Power Connector

1x 9v Battery 

1x Hc-05

M - M

M-F
