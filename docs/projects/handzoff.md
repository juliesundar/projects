
#include <SPI.h>

#include <MFRC522.h>

#include <Wire.h>

#include <LiquidCrystal_I2C.h>

#define SS_PIN 10

#define RST_PIN 9

#define solenoidpin  3    //defines solenoid pin 3

#define ACCESS_DELAY 1000

#define DENIED_DELAY 1000

MFRC522 mfrc522(SS_PIN, RST_PIN);   // Create MFRC522 instance.

LiquidCrystal_I2C lcd(0x27, 16, 2);

void setup()

{

  pinMode(3, OUTPUT); //sets solenoid as Output

  lcd.init();

  // initialize the lcd

  lcd.init();

  // Print a message to the LCD.

  lcd.backlight();

  // Print a message to the LCD.

  lcd.setCursor(0, 0);

  lcd.print("Put your card");

  lcd.setCursor(2, 1);

  lcd.print("to reader");

  SPI.begin();          // Initiate  SPI bus

  mfrc522.PCD_Init();   // Initiate MFRC522

}

void loop()

{

  // Look for new cards

  if ( ! mfrc522.PICC_IsNewCardPresent())

  {

    return;

  }

  // Select one of the cards

  if ( ! mfrc522.PICC_ReadCardSerial())

  {

    return;

  }

  //Show UID on serial monitor

  //lcd.print("UID tag :");

  String content = "";

  byte letter;

  for (byte i = 0; i < mfrc522.uid.size; i++)

  {

    Serial.print(mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " ");

    Serial.print(mfrc522.uid.uidByte[i], HEX);

    content.concat(String(mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " "));

    content.concat(String(mfrc522.uid.uidByte[i], HEX));

  }

  Serial.println();

  Serial.print("Message : ");

  content.toUpperCase();

  if (content.substring(1) == "D9 47 0D BA") //change here the UID of the card

  {

    digitalWrite(3, HIGH);  //sets the solenoid into HIGH state

    delay(2000);            //duration 2 seconds

    digitalWrite(3, LOW); //sets the solenoid into LOW state

    delay(4000);            //duration 4 seconds

    lcd.setCursor(0, 0);

    lcd.print("Access granted");

    lcd.setCursor(2, 1);

    lcd.print("to customer");

  }

  else   {

    lcd.setCursor(0, 0);

    lcd.print("Access denied");

    lcd.setCursor(2, 1);

    lcd.print("to customer");

    delay(DENIED_DELAY)

  }

}
